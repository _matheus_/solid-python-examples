from math import pi


class Circle:
    def __init__(self, radius: float):
        self.radius = radius

    def calc_area(self):
        return pi * pow(self.radius, 2)


class Square:
    def __init__(self, length: float):
        self.length = length

    def calc_area(self):
        return pow(self.length, 2)
