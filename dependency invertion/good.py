class SendGridMailer:
    def __init__(self, api_key, api_secret):
        self.api_key = api_key
        self.api_secret = api_secret

    def send_mail(self, mail):
        print('Enviando email pelo Sendgrid')


class SMTPMailer:
    def __init__(self, username, password, port, host):
        self.username = username
        self.password = password
        self.port = port
        self.host = host

    def send_mail(self, mail):
        print('Enviando email por SMTP')


class EmailService:
    def __init__(self, mail_channel):
        self.mail_channel = mail_channel

    def send(self, mail):
        self.mail_channel.send_mail(mail)


# sendgrid = SendGridMailer('abcd123', '!@$sef23')
smtp = SMTPMailer('meu@email.com', '234DFg', 10, 'email.com')
mail_service = EmailService(smtp)
mail_service.send({})
