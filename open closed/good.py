class Circle:
    def __init__(self, radius: float):
        self.radius = radius

    def draw(self):
        print(f'Desenhando um circulo de raio {self.radius}')


class Square:
    def __init__(self, length: float):
        self.length = length

    def draw(self):
        print(f'Desenhando um quadrado de lado {self.length}')


class ShapeDrawer:
    def draw_shapes(self, shapes: list):
        for shape in shapes:
            shape.draw()


shapes = [Circle(1), Circle(0.5), Square(4), Square(6)]
drawer = ShapeDrawer()
drawer.draw_shapes(shapes)
