class Vehicle:
    def __init__(self, name: str, speed: float):
        self.name = name
        self.speed = speed

    def engine(self):
        pass

    def start_engine(self):
        pass


class Car(Vehicle):
    def start_engine(self):
        pass


class Bicycle(Vehicle):
    def start_engine(self):
        pass
