from shapes import Circle, Square


class AreaSumCalculator:
    def __init__(self, shapes: list):
        self.shapes = shapes

    def sum(self):
        areas = [shape.calc_area() for shape in self.shapes]
        return sum(areas)

    def output_as_str(self):
        return f'Resultado: {self.sum()}'

    def output_as_xml(self):
        return f'<resultado>{self.sum()}</resultado>'

    def output_as_json(self):
        return '{"resultado": %s}' % self.sum()


shapes = [Circle(1), Circle(0.5), Square(4), Square(6)]
calculator = AreaSumCalculator(shapes)


print(calculator.output_as_str())
print(calculator.output_as_json())
print(calculator.output_as_xml())
