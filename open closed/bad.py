class Circle:
    def __init__(self, radius: float):
        self.radius = radius


class Square:
    def __init__(self, length: float):
        self.length = length


class ShapeDrawer:
    def draw_shapes(self, shapes: list):
        for shape in shapes:
            if isinstance(shape, Square):
                print(f'Desenhando um quadrado de lado {shape.length}')
            elif isinstance(shape, Circle):
                print(f'Desenhando um circulo de raio {shape.radius}')


shapes = [Circle(1), Circle(0.5), Square(4), Square(6)]
drawer = ShapeDrawer()
drawer.draw_shapes(shapes)
