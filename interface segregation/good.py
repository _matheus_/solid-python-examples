from abc import ABC, abstractmethod


class Swimmer(ABC):
    @abstractmethod
    def swim(self):
        pass


class Walker(ABC):
    @abstractmethod
    def walk(self):
        pass


class Human(Swimmer, Walker):
    def swim(self):
        pass

    def walk(self):
        pass


class Whale(Swimmer):
    def swim(self):
        pass
