class Vehicle:
    def __init__(self, name: str, speed: float):
        self.name = name
        self.speed = speed


class VehicleWithEngine(Vehicle):
    def engine(self):
        pass

    def start_engine(self):
        pass


class VehicleWithoutEngine(Vehicle):
    def start_moving(self):
        pass


class Car(VehicleWithEngine):
    def engine(self):
        pass

    def start_engine(self):
        pass


class Bicycle(VehicleWithoutEngine):
    def start_moving(self):
        pass
