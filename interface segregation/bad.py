from abc import ABC, abstractmethod


class Mammal(ABC):
    @abstractmethod
    def swim(self):
        pass

    @abstractmethod
    def walk(self):
        pass


class Human(Mammal):
    def swim(self):
        pass

    def walk(self):
        pass


class Whale(Mammal):
    def swim(self):
        pass

    def walk(self):
        pass
