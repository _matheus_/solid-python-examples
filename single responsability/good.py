from shapes import Circle, Square


class AreaSumCalculator:
    def __init__(self, shapes: list):
        self.shapes = shapes

    def sum(self):
        areas = [shape.calc_area() for shape in self.shapes]
        return sum(areas)


class SumCalcOutputter:
    def __init__(self, calculator: AreaSumCalculator):
        self.calculator = calculator

    def as_str(self):
        return f'Resultado: {self.calculator.sum()}'

    def as_xml(self):
        return f'<resultado>{self.calculator.sum()}</resultado>'

    def as_json(self):
        return '{"resultado": %s}' % self.calculator.sum()


shapes = [Circle(1), Circle(0.5), Square(4), Square(6)]
calculator = AreaSumCalculator(shapes)
outputter = SumCalcOutputter(calculator)

print(outputter.as_str())
print(outputter.as_json())
print(outputter.as_xml())
